package com.nabilla.task4;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nabilla.task4.Database.DatabaseAdapter;
import com.nabilla.task4.Model.ModelExpenses;
import com.nabilla.task4.Model.ModelIncome;

import java.util.HashMap;


public class SynchronizeFragment extends Fragment {



    public SynchronizeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new SyncData().execute();
    }

    class SyncData extends AsyncTask<Void, String, Void>{

        ProgressDialog progressDialog;
        DatabaseReference reference;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Synchronize Data");
            progressDialog.setMessage("Please Wait...");
            progressDialog.show();
            reference = FirebaseDatabase.getInstance().getReference("accounting");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            DatabaseAdapter dbAdapter = new DatabaseAdapter(getContext());
            dbAdapter.openDatabase();

            Cursor cursor = dbAdapter.getAllData();
            while (cursor.moveToNext()) {
                String idIncome = "income"+cursor.getString(0);
                String descIncome = cursor.getString(1);
                int amountIncome = cursor.getInt(2);

                String idExpenses = "expenses"+cursor.getString(3);
                String descExpenses = cursor.getString(4);
                int amountExpenses  = cursor.getInt(5);

                ModelIncome modelIncome = new ModelIncome(descIncome, amountIncome);
                ModelExpenses modelExpenses = new ModelExpenses(descExpenses, amountExpenses);

                reference.child(idIncome).setValue(modelIncome);
                reference.child(idExpenses).setValue(modelExpenses);
            }

            dbAdapter.closeDatabase();

            return null;
        }

        @Override
        protected void onPostExecute(Void voidd) {
            super.onPostExecute(voidd);
            progressDialog.cancel();
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                    .setMessage("Success !")
                    .setPositiveButton("Owrait", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setCancelable(false);
            builder.show();
        }
    }
}
