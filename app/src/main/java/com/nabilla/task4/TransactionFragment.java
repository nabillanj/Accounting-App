package com.nabilla.task4;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nabilla.task4.Database.DatabaseAdapter;
import com.nabilla.task4.Model.ModelExpenses;
import com.nabilla.task4.Model.ModelIncome;


public class TransactionFragment extends Fragment {

    EditText edt_desc_expenses, edt_amount_expenses, edt_desc_income, edt_amount_income;
    Button btn_income, btn_outcome;
    DatabaseReference reference;
    RelativeLayout relativeLayout;

    public TransactionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reference = FirebaseDatabase.getInstance().getReference("accounting");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_transaction, container, false);

        edt_desc_expenses = (EditText) view.findViewById(R.id.edt_description_expenses);
        edt_amount_expenses = (EditText) view.findViewById(R.id.edt_amount_expenses);
        edt_desc_income = (EditText) view.findViewById(R.id.edt_description_income);
        edt_amount_income = (EditText) view.findViewById(R.id.edt_amount_income);
        btn_income = (Button) view.findViewById(R.id.btn_income);
        btn_outcome = (Button) view.findViewById(R.id.btn_outcome);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.transaction_layout);

        btn_income.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDataIncome();
            }
        });

        btn_outcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDataExpenses();
            }
        });
        return view;
    }

    public void saveDataIncome(){
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(getContext());
        databaseAdapter.openDatabase();

        String income = edt_amount_income.getText().toString();
        String desc = edt_desc_income.getText().toString();

        if (!TextUtils.isEmpty(income) && !TextUtils.isEmpty(desc)){
            databaseAdapter.addIncome(desc, Integer.parseInt(income));
            Snackbar.make(relativeLayout, "Income inserted !", Snackbar.LENGTH_SHORT).show();
            edt_amount_income.setText("");
            edt_desc_income.setText("");
        }else{
            Snackbar.make(relativeLayout, "Fill all data !", Snackbar.LENGTH_SHORT).show();
        }

        databaseAdapter.closeDatabase();

    }

    public void saveDataExpenses(){
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(getContext());
        databaseAdapter.openDatabase();

        String expenses = edt_amount_expenses.getText().toString();
        String desc = edt_desc_expenses.getText().toString();


        if (!TextUtils.isEmpty(expenses) && !TextUtils.isEmpty(desc)){
            databaseAdapter.addExpense(desc, Integer.parseInt(expenses));
            Snackbar.make(relativeLayout, "Expenses inserted !", Snackbar.LENGTH_SHORT).show();
            edt_amount_expenses.setText("");
            edt_desc_expenses.setText("");
        }else{
            Snackbar.make(relativeLayout, "Fill all data !", Snackbar.LENGTH_SHORT).show();
        }

        databaseAdapter.closeDatabase();
    }
}
