package com.nabilla.task4;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nabilla.task4.Adapter.ListExpensesAdapter;
import com.nabilla.task4.Adapter.ListIncomeAdapter;
import com.nabilla.task4.Database.DatabaseAdapter;
import com.nabilla.task4.Model.ModelExpenses;
import com.nabilla.task4.Model.ModelIncome;

import java.util.ArrayList;
import java.util.List;


public class DashboardFragment extends Fragment {

    TextView tv_total_expenses, tv_total_income, tv_balance;
    List<ModelIncome> incomeList = new ArrayList<>();
    List<ModelExpenses> expensesList = new ArrayList<>();
    ListView listviewExpenses, listviewIncome;
    ListIncomeAdapter adapter;
    ListExpensesAdapter expensesAdapter;
    int income, expenses, balance;

    public DashboardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dashboard, container, false);

        tv_total_expenses = (TextView) view.findViewById(R.id.tv_total_expenses);
        tv_total_income = (TextView) view.findViewById(R.id.tv_total_income);
        listviewExpenses = (ListView) view.findViewById(R.id.listviewExpenses);
        listviewIncome = (ListView) view.findViewById(R.id.listviewIncome);
        tv_balance = (TextView) view.findViewById(R.id.tv_balance);

        getDataIncome();
        getDataExpenses();

        balance = income - expenses;
        tv_balance.setText("Balance : " +balance);

        return view;
    }

    public void getDataIncome(){
        incomeList.clear();
        DatabaseAdapter dbAdapter = new DatabaseAdapter(getContext());
        dbAdapter.openDatabase();

        ModelIncome modelIncome;
        Cursor cursor = dbAdapter.getDataIncome();
        while (cursor.moveToNext()) {
            String id = cursor.getString(0);
            String desc = cursor.getString(1);
            int amount = cursor.getInt(2);

            modelIncome = new ModelIncome();
            modelIncome.setId(id);
            modelIncome.setDescription(desc);
            modelIncome.setAmount(amount);

            incomeList.add(modelIncome);
        }

        Cursor sumCursor = dbAdapter.getSumIncome();
        sumCursor.moveToFirst();
        income = sumCursor.getInt(0);
        tv_total_income.setText(String.valueOf(income));

        if (incomeList.size() > 0){
            adapter = new ListIncomeAdapter(getContext(), incomeList);
            listviewIncome.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Log.d("ADAPTER_LIST", String.valueOf(incomeList.size()));
        }

        Log.d("TEST", String.valueOf(incomeList.size()));

        dbAdapter.closeDatabase();
    }

    public void getDataExpenses(){
        DatabaseAdapter dbAdapter = new DatabaseAdapter(getContext());
        dbAdapter.openDatabase();

        ModelExpenses modelExpenses;
        Cursor cursor = dbAdapter.getDataExpenses();
        while (cursor.moveToNext()){
            String id = cursor.getString(0);
            String desc = cursor.getString(1);
            int amount = cursor.getInt(2);

            modelExpenses = new ModelExpenses();
            modelExpenses.setId(id);
            modelExpenses.setDescription(desc);
            modelExpenses.setAmount(amount);

            expensesList.add(modelExpenses);
        }

        Cursor sumCursor = dbAdapter.getSumExpenses();
        sumCursor.moveToFirst();
        expenses = sumCursor.getInt(0);
        tv_total_expenses.setText(String.valueOf(expenses));

        if (expensesList.size() > 0){
            expensesAdapter = new ListExpensesAdapter(getContext(), expensesList);
            listviewExpenses.setAdapter(expensesAdapter);
            expensesAdapter.notifyDataSetChanged();
            Log.d("TEST", String.valueOf(expensesList.size()));
        }

        dbAdapter.closeDatabase();
    }
}
