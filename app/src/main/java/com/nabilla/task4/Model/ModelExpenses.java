package com.nabilla.task4.Model;


public class ModelExpenses {
    private String id;
    private String description;
    private int amount;

    public ModelExpenses() {
    }

    public ModelExpenses(String description, int amount) {
        this.description = description;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
