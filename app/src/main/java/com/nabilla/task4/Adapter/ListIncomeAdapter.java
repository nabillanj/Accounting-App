package com.nabilla.task4.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nabilla.task4.Model.ModelIncome;
import com.nabilla.task4.R;

import java.util.ArrayList;
import java.util.List;

public class ListIncomeAdapter extends BaseAdapter{

    Context context;
    List<ModelIncome> modelIncomeList = new ArrayList<>();

    public ListIncomeAdapter(Context context, List<ModelIncome> modelIncomeList) {
        this.context = context;
        this.modelIncomeList = modelIncomeList;
    }

    @Override
    public int getCount() {
        return modelIncomeList.size();
    }

    @Override
    public Object getItem(int i) {
        return modelIncomeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return modelIncomeList.indexOf(modelIncomeList.get(i));
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list, viewGroup, false);
            holder = new ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            holder.txtAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        ModelIncome modelIncome = (ModelIncome) getItem(i);

        holder.txtDesc.setText(modelIncome.getDescription());
        holder.txtAmount.setText(String.valueOf(modelIncome.getAmount()));

        return convertView;
    }

    private class ViewHolder{
        TextView txtDesc, txtAmount;
    }
}
