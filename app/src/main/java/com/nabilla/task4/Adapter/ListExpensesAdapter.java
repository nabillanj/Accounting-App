package com.nabilla.task4.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nabilla.task4.Model.ModelExpenses;
import com.nabilla.task4.Model.ModelIncome;
import com.nabilla.task4.R;

import java.util.ArrayList;
import java.util.List;

public class ListExpensesAdapter extends BaseAdapter{

    private Context context;
    private List<ModelExpenses> modelExpensesList = new ArrayList<>();

    public ListExpensesAdapter(Context context, List<ModelExpenses> modelExpensesList) {
        this.context = context;
        this.modelExpensesList = modelExpensesList;
    }

    @Override
    public int getCount() {
        return modelExpensesList.size();
    }

    @Override
    public Object getItem(int i) {
        return modelExpensesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return modelExpensesList.indexOf(modelExpensesList.get(i));
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ListExpensesAdapter.ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list, viewGroup, false);
            holder = new ListExpensesAdapter.ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            holder.txtAmount = (TextView) convertView.findViewById(R.id.tv_amount);
            convertView.setTag(holder);
        }
        else {
            holder = (ListExpensesAdapter.ViewHolder) convertView.getTag();
        }

        ModelExpenses modelExpenses = (ModelExpenses) getItem(i);

        holder.txtDesc.setText(modelExpenses.getDescription());
        holder.txtAmount.setText(String.valueOf(modelExpenses.getAmount()));

        return convertView;
    }

    private class ViewHolder{
        TextView txtDesc, txtAmount;
    }
}
