package com.nabilla.task4.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(Context context) {
        super(context, "DB_TASK_4", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String tableIncome = "" +
                "CREATE TABLE t_income(" +
                "id_income INTEGER PRIMARY KEY AUTOINCREMENT," +
                "description VARCHAR(30)," +
                "amount INTEGER)";
        String tableExpenses = "" +
                "CREATE TABLE t_expenses(" +
                "id_expenses INTEGER PRIMARY KEY AUTOINCREMENT," +
                "description VARCHAR(30)," +
                "amount INTEGER)";

        sqLiteDatabase.execSQL(tableIncome);
        sqLiteDatabase.execSQL(tableExpenses);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST t_expenses");
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST t_income");

        onCreate(sqLiteDatabase);
    }
}
