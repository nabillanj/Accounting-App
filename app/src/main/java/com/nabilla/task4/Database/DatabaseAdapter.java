package com.nabilla.task4.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAdapter {

    private Context context;
    private SQLiteDatabase database;
    private DatabaseHelper helper;

    public DatabaseAdapter(Context context) {
        this.context = context;
        helper = new DatabaseHelper(context);
    }

    public DatabaseAdapter openDatabase(){
        database = helper.getWritableDatabase();
        return this;
    }

    public void closeDatabase(){
        helper.close();
    }

    public void addIncome(String desc, int amount){
        ContentValues cv = new ContentValues();
        cv.put("description", desc);
        cv.put("amount", amount);

        database.insert("t_income", "id_income", cv);

    }

    public void addExpense(String desc, int amount){
        ContentValues cv = new ContentValues();
        cv.put("description", desc);
        cv.put("amount", amount);

        database.insert("t_expenses", "id_expenses", cv);
    }

    public Cursor getDataIncome(){

        return database.rawQuery("SELECT * FROM t_income", null);
    }

    public Cursor getDataExpenses(){
        return database.rawQuery("SELECT * FROM t_expenses", null);
    }

    public Cursor getSumIncome(){
        return database.rawQuery("SELECT SUM(amount) FROM t_income", null);
    }

    public Cursor getSumExpenses(){
        return database.rawQuery("SELECT SUM(amount) FROM t_expenses", null);
    }

    public Cursor getAllData(){
        return database.rawQuery("SELECT * FROM t_income, t_expenses", null);
    }
}
